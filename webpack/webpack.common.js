const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const basePath = path.resolve(__dirname, '../');
const APP_DIR = path.resolve(basePath, './src');

const phaserModule = path.join(basePath, '/node_modules/phaser/');
const phaser = path.join(phaserModule, 'build/custom/phaser-split.js');
const pixi = path.join(phaserModule, 'build/custom/pixi.js');
const p2 = path.join(phaserModule, 'build/custom/p2.js');

module.exports = {
  entry: {
    app: [
      'babel-polyfill',
      `${APP_DIR}/js/game.js`
    ]
  },
  output: {
    path: path.resolve(basePath, 'dist/'),
    filename: '[name].bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Production',
      template: './src/index.html'
    }),
    new CleanWebpackPlugin(['dist'], {
      root: `${basePath}`
    })
  ],
  resolve: {
    alias: { pixi, p2, phaser }
  },
  module: {
    rules: [
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      { test: /\.(png|svg|jpg|gif)$/, use: ['file-loader'] },
      { test: /\.(woff|woff2|eot|ttf|otf)$/, use: ['file-loader'] },
      { test: /\.(ogg|mp3)$/, use: ['file-loader'], include: APP_DIR },
      { test: /\.js$/, use: ['babel-loader'], include: APP_DIR },
      { test: /pixi\.js/, use: ['expose-loader?PIXI'] },
      { test: /phaser-split\.js$/, use: ['expose-loader?Phaser'] },
      { test: /p2\.js/, use: ['expose-loader?p2'] }
    ]
  }
};
