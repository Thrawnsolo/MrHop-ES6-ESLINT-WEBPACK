import pixi from 'pixi';
import p2 from 'p2';
import phaser from 'phaser';
import BootState from './states/bootState';
import PreloadState from './states/preloadState';
import GameState from './states/gameState';

class Game extends phaser.Game {
  constructor() {
    const width = 1024;
    const height = 768;
    super(width, height, phaser.CANVAS);
    this.state.add('Boot', BootState, false);
    this.state.add('Preload', PreloadState, false);
    this.state.add('GameState', GameState, false);
  }
}
window.game = new Game();
window.game.state.start('Boot');
