import phaser from 'phaser';

export default class Platform extends phaser.Group {
  prepare(numTiles, x, y, speed, coinsPool) {
    let i = 0;
    this.alive = true;
    while (i < numTiles) {
      let floorTile = this.floorPool.getFirstExists(false);
      if (!floorTile) {
        floorTile = new phaser.Sprite(this.game, x + (i * this.tileSize), y, 'floor');
      } else {
        floorTile.reset(x + (i * this.tileSize), y);
      }
      this.add(floorTile);
      i += 1;
    }
    this.setAll('body.immovable', true);
    this.setAll('body.allowGravity', false);
    this.setAll('body.velocity.x', -speed);
    this.addCoins(speed, coinsPool);
  }
  addCoins(speed, coinsPool) {
    const coinsY = 90 + (Math.random() * 110);
    let hasCoin;
    this.forEach((tile) => {
      hasCoin = Math.random() <= 0.4;
      if (hasCoin) {
        let coin = coinsPool.getFirstExists(false);
        if (!coin) {
          coin = new phaser.Sprite(this.game, tile.x, tile.y - coinsY, 'coin');
          coinsPool.add(coin);
        } else {
          coin.reset(tile.x, tile.y - coinsY);
        }
        coin.body.velocity.x = -speed;
        coin.body.allowGravity = false;
      }
    });
  }
  constructor(parameters) {
    super(window.game);
    this.game = window.game;
    this.floorPool = parameters.floorPool;
    this.numTiles = parameters.numTiles;
    this.levelSpeed = parameters.levelSpeed;
    this.coinsPool = parameters.coinsPool;
    this.tileSize = 40;
    this.enableBody = true;
    this.prepare(this.numTiles, parameters.x, parameters.y, this.levelSpeed, this.coinsPool);
  }

  // Phaser Groups does not have kill method
  // We need to kill the sprites and then move them to the floorPool group, in order to be reused
  // We also need an auxiliar array to move the sprites being used to the floorPool again
  kill() {
    const sprites = [];
    this.alive = false;
    // This method kills all the sprites, callAll makes the children to call the kill method,
    // that all the sprites have.
    this.callAll('kill');
    // Get all the tiles in the group
    this.forEach((tile) => {
      sprites.push(tile);
    });
    // And then reuse it.
    sprites.forEach((sprite) => {
      this.floorPool.add(sprite);
    }, this);
  }
}

