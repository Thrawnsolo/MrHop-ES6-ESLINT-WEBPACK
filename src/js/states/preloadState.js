import phaser from 'phaser';
import playerDeadImage from '../../png/player_dead.png';
import floorImage from '../../png/floor.png';
import waterImage from '../../png/water.png';
import coinImage from '../../png/coin.png';
import backgroundImage from '../../png/background.png';
import playerSpriteSheetImage from '../../png/player_spritesheet.png';
import coinAudioOgg from '../../audio/coin.ogg';
import coinAudioMp3 from '../../audio/coin.mp3';

export default class Preload extends phaser.State {
  preload() {
    this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadbar');
    this.preloadBar.anchor.setTo(0.5);
    this.preloadBar.scale.setTo(3);

    this.load.setPreloadSprite(this.preloadBar);

    this.load.image('playerDead', playerDeadImage);
    this.load.image('floor', floorImage);
    this.load.image('water', waterImage);
    this.load.image('coin', coinImage);
    this.load.image('background', backgroundImage);
    this.load.spritesheet('player', playerSpriteSheetImage, 51, 67, 5, 2, 3);
    this.load.audio('coin', [coinAudioMp3, coinAudioOgg]);
  }
  create() {
    this.state.start('GameState');
  }
}
