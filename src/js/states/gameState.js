import phaser from 'phaser';
import Platform from '../prefabs/platform';

export default class GameState extends phaser.State {
  create() {
    // moving background
    this.levelSpeed = 200;
    this.background = this.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'background');
    this.background.tileScale.y = 4.82;
    this.background.autoScroll(-this.levelSpeed / 6, 0);
    this.game.world.sendToBack(this.background);
    this.coinSound = this.add.audio('coin');
    this.floorPool = this.add.group();
    this.platformPool = this.add.group();
    this.coinsPool = this.add.group();
    this.coinsPool.enableBody = true;
    // Gravity
    this.game.physics.arcade.gravity.y = 1000;
    this.maxJumpDistance = 120;
    this.cursors = this.game.input.keyboard.createCursorKeys();
    this.myCoins = 0;
    this.player = this.add.sprite(50, (this.game.world.height / 2) - 40, 'player');
    this.player.anchor.setTo(0.5);
    this.player.animations.add('running', [0, 1, 2, 3, 2, 1], 15, true);
    this.game.physics.arcade.enable(this.player);
    this.player.body.setSize(35, 58, 8, 5);
    this.player.play('running');

    this.currentPlatform = new Platform({
      floorPool: this.floorPool,
      numTiles: 10,
      x: 0,
      y: (this.game.world.height / 2) + 40,
      levelSpeed: this.levelSpeed,
      coinsPool: this.coinsPool
    });

    this.platformPool.add(this.currentPlatform);

    const secondPlatformData = this.generateRandomPlatform();
    const currentPlatformLength = this.currentPlatform.length;
    const currentPlatformChildren = this.currentPlatform.children;

    // TODO: create an initial landscape that
    // covers the whole screen, like currentplatform.right + nextPlatform right ...
    // === world.width
    this.platformPool.add(new Platform({
      floorPool: this.floorPool,
      numTiles: secondPlatformData.numTiles + 6,
      x: currentPlatformChildren[currentPlatformLength - 1].right + secondPlatformData.separation,
      y: secondPlatformData.y,
      levelSpeed: this.levelSpeed,
      coinsPool: this.coinsPool
    }));

    this.createPlatform();

    this.water = this.add.tileSprite(0, this.game.world.height - 30, this.game.world.width, 30, 'water');
    this.water.autoScroll(-this.levelSpeed / 4, 0);

    // show number of coins
    const style = { font: '30px Arial', fill: '#fff' };
    this.coinsCountLabel = this.add.text(10, 20, '0', style);
  }
  collectCoin(player, coin) {
    coin.kill();
    this.myCoins += 1;
    this.coinSound.play();
    this.coinsCountLabel.text = this.myCoins;
  }
  update() {
    if (this.player.alive) {
      const currentPlatformChildren = this.currentPlatform.children;
      this.platformPool.forEachAlive((platform) => {
        this.game.physics.arcade.collide(this.player, platform);
        if (platform.length && platform.children[platform.length - 1].right < 0) {
          platform.kill();
        }
      });

      this.game.physics.arcade.overlap(this.player, this.coinsPool, this.collectCoin, null, this);

      this.coinsPool.forEachAlive((coin) => {
        if (coin.right < 0) {
          coin.kill();
        }
      });

      // keep in mind that we also need to add velocity to the player, so it actually runs
      this.player.body.velocity.x = this.player.body.touching.down ? this.levelSpeed : 0;

      if (this.cursors.up.isDown || this.game.input.activePointer.isDown) {
        this.playerJump();
      } else if (this.cursors.up.isUp || this.game.input.activePointer.isUp) {
        this.isJumping = false;
      }

      if (this.currentPlatform.length &&
        currentPlatformChildren[this.currentPlatform.length - 1].right < this.game.world.width) {
        this.createPlatform();
      }
      if (this.player.top >= this.game.world.height || this.player.left <= 0) {
        this.gameOver();
      }
    }
  }

  gameOver() {
    this.player.kill();
    this.updateHighScore();
    this.overlay = this.add.bitmapData(this.game.width, this.game.height);
    this.overlay.ctx.fillStyle = '#000';
    this.overlay.ctx.fillRect(0, 0, this.game.width, this.game.height);
    this.panel = this.add.sprite(0, this.game.height, this.overlay);
    this.panel.alpha = 0.5;
    const gameOverPanel = this.add.tween(this.panel);
    gameOverPanel.to({ y: 0 }, 500);
    gameOverPanel.onComplete.add(() => {
      const gameOverStyle = { font: '30px Arial', fill: '#fff' };
      const textStyle = { font: '20px Arial', fill: '#fff' };
      const tapTextStyle = { font: '18px Arial', fill: '#fff' };
      this.water.stopScroll();
      this.background.stopScroll();

      this.add.text(
        this.game.width / 2,
        this.game.height / 2,
        'GAME OVER',
        gameOverStyle
      ).anchor.setTo(0.5);

      this.add.text(
        this.game.width / 2,
        (this.game.height / 2) + 50,
        `High Score: ${this.highScore}`,
        textStyle
      ).anchor.setTo(0.5);

      this.add.text(
        this.game.width / 2,
        (this.game.height / 2) + 80,
        `Your Score: ${this.myCoins}`,
        textStyle
      ).anchor.setTo(0.5);

      this.add.text(
        this.game.width / 2,
        (this.game.height / 2) + 110,
        'Tap to play again',
        tapTextStyle
      ).anchor.setTo(0.5);

      this.game.input.onDown.addOnce(this.restart, this);
    });
    gameOverPanel.start();
  }

  restart() {
    // We have to deal with a bug in phaser 3.3
    // Thats why we remove the background and the water
    this.game.world.remove(this.background);
    this.game.world.remove(this.water);
    this.game.state.start('GameState');
  }

  updateHighScore() {
    this.highScore = +localStorage.getItem('highScore');
    if (this.highScore < this.myCoins) {
      this.highScore = this.myCoins;
      localStorage.setItem('highScore', this.highScore);
    }
  }

  playerJump() {
    // TODO Fix the bug where you can actually jump again when collecting a coin
    if (this.player.body.touching.down && !this.isJumping) {
      this.startJumpY = this.player.y;
      this.isJumping = true;
      this.jumpPeaked = false;
      this.player.body.velocity.y = -300;
    } else if (this.isJumping && !this.jumpPeaked) {
      const distanceJumped = this.startJumpY - this.player.y;
      if (distanceJumped <= this.maxJumpDistance) {
        this.player.body.velocity.y = -300;
      } else {
        this.jumpPeaked = true;
      }
    }
  }

  generateRandomPlatform() {
    const data = {};
    const minSeparation = 60;
    const maxSeparation = 180;
    const currentPlatformChildren = this.currentPlatform.children;

    data.separation = minSeparation + (Math.random() * (maxSeparation - minSeparation));
    // y in regards to the previous platform
    const minDifY = -120;
    const maxDifY = 120;

    data.y = currentPlatformChildren[0].y + minDifY + (Math.random() * (maxDifY - minDifY));
    data.y = Math.max(150, data.y);
    data.y = Math.min(this.game.world.height - 50, data.y);

    // number of tiles
    const minTiles = 1;
    const maxTiles = 5;
    data.numTiles = minTiles + (Math.random() * (maxTiles - minTiles));

    return data;
  }
  createPlatform() {
    const nextPlatformData = this.generateRandomPlatform();
    if (nextPlatformData) {
      this.currentPlatform = this.platformPool.getFirstDead();
      if (this.currentPlatform) {
        this.currentPlatform.prepare(
          nextPlatformData.numTiles,
          this.game.world.width + nextPlatformData.separation,
          nextPlatformData.y,
          this.levelSpeed,
          this.coinsPool
        );
      } else {
        this.currentPlatform = new Platform({
          floorPool: this.floorPool,
          numTiles: nextPlatformData.numTiles,
          x: this.game.world.width + nextPlatformData.separation,
          y: nextPlatformData.y,
          levelSpeed: this.levelSpeed,
          coinsPool: this.coinsPool
        });
      }
      this.platformPool.add(this.currentPlatform);
      this.currentIndex += 1;
    }
  }
}
